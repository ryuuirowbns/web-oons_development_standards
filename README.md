# Estándares Web-oOns
Este repositorio sirve para mantener toda la documentación y utilidades que los desarrolladores de WEB-OONS utilizan para realizar sus tareas.

Las palabras clave "DEBE" (MUST), NO DEBE (MUST NOT, "REQUERIDO" (REQUIRED), "DEBERÁ" (SHALL), "NO DEBERÁ" (SHALL NOT), "DEBERÍA" (SHOULD),
"NO DEBE" (SHOULD NOT), "RECOMENDADO" (RECOMMENDED), "PUEDE" (MAY), y "OPCIONAL" (OPTIONAL) en este documento deben ser
interpretadas como se describe en el [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

## Seguridad
### Bitbucket
Web-oOns utiliza Bitbucket para su alojamiento de código. Por razones de seguridad, todos los desarrolladores DEBEN activar 2FA (Two-Factor Authentication) en Bitbucket.

Si actualmente no tienes un cliente de OTP (One time password), como [Authy](https://www.authy.com) o Google Authenticator, échale un vistazo a [Authy](https://www.authy.com). Está disponible en Android, iOS y Chrome.

## Advertencias
Se incluyen en el directorio de estándares varios documentos que DEBEN seguirse. El desviarse de estos PUEDE dar lugar a medidas disciplinarias.


### standards
Este directorio incluye el conjunto de documentos de estándares que todos los desarrolladores de Web-oOns DEBEN cumplir.

## Estándares

### [Estándares para commit](standards/commit.md)
Este archivo documenta los estándares que todos los desarrolladores de Web-oOns DEBEN usar para la utilización de Git.

### [Estándares de codificación PHP](standards/php.md)
Este archivo documenta los estándares que todo el código PHP de Web-oOns debe seguir. 

### [Estándares de codificación JavaScript](standards/javascript.md)
Este archivo documenta los estándares que todo el código JavaScript de Web-oOns debe seguir.
