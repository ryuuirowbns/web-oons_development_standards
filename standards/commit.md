# Estándares de WEB-OONS para commit
Hay muchas maneras de manejar commits. El propósito de este documento es definir de forma restrictiva cómo se manejan en WEB-OONS.

Las palabras clave "DEBE" (MUST), NO DEBE (MUST NOT, "REQUERIDO" (REQUIRED), "DEBERÁ" (SHALL), "NO DEBERÁ" (SHALL NOT), "DEBERÍA" (SHOULD),
"NO DEBE" (SHOULD NOT), "RECOMENDADO" (RECOMMENDED), "PUEDE" (MAY), y "OPCIONAL" (OPTIONAL) en este documento deben ser
interpretadas como se describe en el [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

## Contenido de commit
Un commit DEBERÍA ser un conjunto específico de cambios relacionados. Los commits DEBEN ser capaces de revertirse sin causar efectos secundarios.

## Mensajes
La primer línea de un mensaje de commit, también conocido como "subject", DEBE ser una frase completa. La frase
NO DEBE tener más de 50 caracteres. Si necesitas incluir más información, se DEBE utilizar un mensaje secundario,
también conocido como "body". El body DEBE seguir la gramática correcta en "inglés o español", y las líneas NO DEBEN exceder los 72 caracteres.

Ejemplo:

```
Redireccionar usuario a la página correcta después de iniciar sesión

https://trello.com/path/to/relevant/card

Los usuarios se estaban redirigiendo al home al iniciar sesión, lo que es
menos útil que redirigir a la página que habían solicitado originalmente
antes de ser redirigidos al formulario de inicio de sesión.

* Almacenar la ruta solicitada en una variable de sesión
* Redireccionar a la ubicación guardada después de iniciar sesión
```

La razón de esto es hacer que las historias del commit sean fáciles de revisar visualmente como mensajes de una línea. Si la esencia de un cambio
no se puede comunicar en la primer línea, esa habilidad se pierde.

### Prefijo del problema
Si el proyecto en el que estás trabajando está usando un software de administración de proyectos (por ejemplo, Jira), los commits PUEDEN ser prefijados con el problema relacionado con este. Un ejemplo es `[PROJ-1234] Nuevo focus agregado.`. Dependiendo del proyecto se determinará si este prefijo debe ser utilizado o no.


## ¿Cuándo hacer un commit?
### Frecuencia
Se DEBE commitear con frecuencia, esto no significa que necesitas hacer commits cada 5 minutos. Simplemente piensa en ello como una forma de guardar el código completo. Si no lo haces, no se puede revertir si rompes algo.

### Fin del día
DEBES commitear y hacer push de todos tus cambios antes de salir al final del día. Si algo le pasa a su computadora portátil o a tí, la empresa no debe perder el acceso a los cambios que estaban en curso. Si tienes un trabajo en progreso, se DEBE prefijar su mensaje de commit con `[WIP]`. Un ejemplo es `[WIP] Cambio de vista en player agregado.`.

## Historial de rama
NO PUEDES hacer squash de los cambios en un PR para proyectos a menos que se le solicite hacerlo. El squashing elimina nuestra capacidad de ver lo que ha cambiado durante un PR.

NO PUEDES, hacer un ammend en un commit y hacer un push forzado cuando un PR es abierto a menos que se especifique lo contrario.

## ¿Se puede hacer merge?
A una rama NO DEBES realizarle merge a menos que todas las pruebas pasen. Si una prueba falla, pero cae fuera del alcance de la rama, debe ser discutida con el líder del equipo en cuanto a si debe o no hacerse fix dentro de la rama actual.

## ¿Cómo hago merge?
Debes solicitar un PR si tu código se en cuentra completo, lo has revisado y todas las pruebas pasan. Si necesitas tener una discusión abierta sobre algo en progreso, DEBES prefijar el PR con `[WIP]`.

DEBES poner el prefijo del problema en el título PR  (por ejemplo, `[PROJ-1234] Nuevo player HTML5 creado.`)

## Referencia de prefijos
- [WIP] - Work in Progress (ejemplo, `[WIP] Cambio de vista en player agregado.`)
- [PROBLEMA] - Problema relacionado con el commit (ejemplo, `[PROJ-1234] Nuevo player HTML5 creado.`)

## Herramientas
Un proyecto PUEDE proporcionar un archivo [commit template](http://codeinthehole.com/writing/a-useful-template-for-commit-messages/)
para ayudar a los desarrolladores a recordar el formato. 
Un desarrollador PUEDE utilizar el hook [Fit Commit](https://github.com/m1foley/fit-commit)
para validar los mensajes de confirmación.
