Estandar PHP Web-oOns
-------------------------

Esta guía se extiende y se expande en [SYMFONY](http://symfony.com/doc/current/contributing/code/standards.html), que se expande en [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md), la comunidadthe PHP community coding standard.

The intent of this guide is to reduce cognitive friction when scanning code
from different authors. It does so by enumerating a shared set of rules and
expectations about how to format PHP code.

Las palabras clave "DEBE" (MUST), NO DEBE (MUST NOT, "REQUERIDO" (REQUIRED), "DEBERÁ" (SHALL), "NO DEBERÁ" (SHALL NOT), "DEBERÍA" (SHOULD),
"NO DEBE" (SHOULD NOT), "RECOMENDADO" (RECOMMENDED), "PUEDE" (MAY), y "OPCIONAL" (OPTIONAL) en este documento deben ser
interpretadas como se describe en el [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

## Referencias
* [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt)
* [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md)
* [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)
* [PSR-4](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4.md)
* [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design))

## Docblocks y Comentarios
* Los docblocks DEBEN usar la sintaxis de [phpDocumentor](http://www.phpdoc.org/)
* El docblock DEBE contener en el siguiente formato: mensaje, anotaciones, definiciones de parámetros, definición de excepciones, definición de retorno (return)
```php
/**
 * Este método hace algo.
 *
 * @Anotacion()
 * @Anotacion()
 *
 * @param Type $variable
 * @param Type $variable2
 *
 * @throws InvalidArgumentException
 * @throws InvalidArgumentException
 * @throws OtherException
 *
 * @return Type1|Type2
 */
```
* Las secciones previamente definidas DEBEN estar separadas por una nueva línea.
* Los métodos NO DEBEN tener un docblock, a menos que las siguientes condiciones sean verdaderas.
    * El método arroja una excepción
    * El método tiene una anotación
    * Un parámetro del método no puede ser tipo insinuado debido a que es un tipo de unión o mixto
    * Un parámetro del método necesita una anulación de tipo en el bloque de documentos, como anular un tipo de matriz con una clase
* Si tales condiciones son verdaderas, DEBEN documentarse sólo las condiciones necesarias.
* Todas las propiedades DEBEN tener un docblock.
* DEBEN evitarse los comentarios redundantes.
    * ej: `Este método obtiene algo`,` método que agrega algo`
* Los docblocks DEBEN usar importaciones para todas las clases.
* Los docblocks DEBERÍAN declarar todas y cada una de las instancias de todas las excepciones que arroje un método.
* Un docblock en línea DEBE definirse en la línea antes de la variable.
```
/** Type $variable */
$variable = ...;
```
* Los tipos de colección de objetos DEBEN usar la sintaxis de dos brackets.
    * ej: `DateTime[]`, `User[]`, etc.
* Los comentarios en línea DEBEN usar barras dobles.
* Los comentarios de tareas DEBEN colocarse donde serán implementado/llamado.

## Namespaces e Importación de clases
* El código DEBE usar una declaración de uso por línea (es decir, no use la sintaxis de grupo de uso).
* El código DEBE importar todas las clases.
* los namespaces DEBERÍAN ser singulares.

## Variables
* Las variables DEBERÍAN ser descriptivas (ej .. $userId no $ key, $user no $value).

## Nuevas líneas
* Una nueva línea DEBERÍA colocarse antes y después de un bloque de código.
Una nueva línea entre una asignación y un bloque de código que interactúa con la variable es OPCIONAL.
```php
public function methodWithSpace()
{
    $var = false;

    if ($var === true) {
        $this->doSomething();
    }

    $this->doSomethingElse();
}
```
```php
public function methodWithoutSeperation()
{
    $var = false;
    if ($var === true) {
        $this->doSomething();
    }

    $this->doSomethingElse();
}
```
* Se PUEDE colocar una nueva línea después de declarar una variable y cualquier lógica comercial, pero es OPCIONAL
```php
$var = new Something();

$var->doSomething();
$var->doSomethingElse();
```
```php
$var = new Something();
$var->doSomething();
$var->doSomethingElse();
```
* Una nueva línea DEBE ubicarse entre una nueva línea entre la lógica de negocios no relacionada
```php
$this->handle(new Something());

$this->addFlash('success', 'Some Message');
```

## Cadenas
* Las cadenas DEBEN usar siempre comillas simples
* La concatenación DEBE hacerse solo mediante el uso de la función `sprintf ()`, a menos que la variable se coloque al inicio o al final de la cadena
    * ej: `'Interpolación de cadenas:'. $var
* Las nuevas líneas DEBEN escribirse con `PHP_EOL` en lugar de` \ n`
* Todas las consultas SQL DEBEN escribirse dentro de comillas simples

## Arrays
* Todas las declaraciones DEBEN usar sintaxis corta
* Se DEBE insertar una coma en el último elemento, a menos que sea un arreglo en línea
* NO DEBE alinear keys y valores dentro de las matrices (es decir, DEBE conservar solo un espacio antes y un espacio después del operador de flecha `=>`)

## Clases
* Los prefijos/sufijos NO DEBEN ser utilizados en nombres de clase, incluyendo rasgos e interfaces en la mayoría de los casos. En cambio, trate de usar namespaces y nombres de clases descriptivas. Esto ayuda a mantener las clases alineadas con una sola responsabilidad.
    * Ejemplos:
        * `CompileUserPermissions` instead of `UserPermissionService`
        * `CastsToJson` instead of `JsonInterface`
    * Las excepciones a esto incluyen excepciones, y también clases como repositorios, donde el nombre de clase no puede llamarse de manera realista para ser comprensible por sí mismo.
        * Las excepciones DEBEN terminar en `Exception` (ej.` InvalidArgumentException`)
    * Otra excepción son los rasgos cuyo principal objetivo es hacer que la inyección de dependencia sea reutilizable, como `SessionAware`.
* Las constantes DEBEN tener declaración visible.
* Los métodos y la visibilidad de la propiedad DEBEN declararse como protegidos en las bibliotecas.
    * En el código específico de su aplicación, depende del líder del proyecto definir el estándar.
* El orden RECOMENDADO de los elementos de clase es: rasgos, constantes, propiedades, constructor, destructor, métodos públicos, métodos protegidos, métodos privados, getters/setters.
* Getters y setters DEBEN estar agrupados por propiedad respectiva.
* Getters y setters DEBEN declararse en el orden de sus respectivas propiedades.
* Todas las propiedades deberían tener un valor de defecto razonable si se puede proporcionar (por ejemplo, una lista debe tener un array vacío).
* Tener un constructor inmanejable indica que la clase es probablemente demasiado compleja y no sigue los principios SOLID.

```php
class User
{
    public const PASSWORD_EXPIRATION_LENGTH = 3;
    public const TYPE_ADMIN = 'admin';

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var bool
     */
    private $enabled = true;

    public function __construct(string $name, string $email, bool $enabled = true)
    {
        $this->name = $name;
        $this->email = $email;
        $this->enabled = true;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct()
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function __get(string $attribute)
    {
        return $this->$attribute ?? null;
    }

    public function __set(string $attribute, $value): void
    {
        $this->$attribute = $value;
    }
    
    public function something()
    {
        // TODO: Implement something()
    }

}
```
## Métodos & Funciones
* Las funciones y métodos DEBEN tener definiciones type para parámetros y retornar un type.
Esto incluye `void` para funciones/métodos sin valor de retorno.
    * Las excepciones son solo para types mixtos y de unión.
* `return;` DEBE usarse SOLAMENTE para returns `void`.
* `return null;` DEBE utilizarse cuando se retorna `null`.
* El código NO DEBE usar interfaces fluidas.

## Genérico
* Todas las tabulaciones DEBEN ser usando 4 espacios.
* NO SE DEBEN reutilizar variables ya declaradas.
* Las llamadas a métodos PUEDEN usarse como argumentos solo cuando no tienen parámetros o no arrojan excepciones.
* Cuando se vinculan parámetros en `PDO`, se DEBE usar parámetros nombrados en lugar de los numerados.
* La función `date ()` NO DEBE utilizarse cuando se manipulen fechas, solo se obtiene una cadenas.

## Base de datos
* Se DEBE usar palabras en plural para los nombres de las tablas.
* Se DEBE usar snake case para nombrar columnas.

