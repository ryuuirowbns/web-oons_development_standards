Estándares Javascript Web-oOns
-------------------------
Este enlace a la guía [AIRBNB](https://github.com/airbnb/javascript).
Web-oOns no mantiene sus propios estándares de JavaScript.
En cambio, optamos por utilizar los bien escritos y fundados "JavaScript Airbnb standards".

El objetivo de esta guía es reducir la fricción cognitiva cuando se revisa el código
de diferentes autores. Lo hace al enumerar un conjunto compartido de reglas y
expectativas sobre cómo formatear el código JavaScript.

Las palabras clave "DEBE" (MUST), NO DEBE (MUST NOT, "REQUERIDO" (REQUIRED), "DEBERÁ" (SHALL), "NO DEBERÁ" (SHALL NOT), "DEBERÍA" (SHOULD),
"NO DEBE" (SHOULD NOT), "RECOMENDADO" (RECOMMENDED), "PUEDE" (MAY), y "OPCIONAL" (OPTIONAL) en este documento deben ser
interpretadas como se describe en el [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

## Referencias
* [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt)
* [AIRBNB](https://github.com/airbnb/javascript)